public class main {
    public static void main(String args[])
    {
        String stringToIterate = "New York";

        for (int i=0;i<stringToIterate.length();i++) {
            System.out.println(i + ": " +showChar(stringToIterate, i));
        }
    }

    public static char showChar(String string, int position)
    {
        return string.charAt(position);
    }
}
